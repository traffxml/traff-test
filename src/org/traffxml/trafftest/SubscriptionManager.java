package org.traffxml.trafftest;

import org.traffxml.traff.Version;
import org.traffxml.traff.subscription.Capabilities;
import org.traffxml.traff.subscription.FilterList;
import org.traffxml.transport.android.AndroidTransport;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * A receiver for TraFF poll requests.
 */
public class SubscriptionManager extends BroadcastReceiver {
	static final String TAG = "SubscriptionManager";
	static final int MIN_VERSION = Version.V0_7;
	static final int TARGET_VERSION = Version.V0_8;
	static final Capabilities CAPABILITIES = new Capabilities(MIN_VERSION, TARGET_VERSION);

    /* (non-Javadoc)
     * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
     */
    @Override
    public void onReceive(Context context, Intent intent) {
		FilterList filterList = null;
		String subscriptionId;
		Bundle resultExtras;
		Intent logIntent = new Intent(Util.ACTION_INTERNAL_LOG);
		if (intent != null) {
			StringBuilder builder = new StringBuilder();
			builder.append("Source: ").append(intent.getAction());
			switch (intent.getAction()) {
			case AndroidTransport.ACTION_TRAFF_GET_CAPABILITIES:
				Log.d(TAG, builder.toString());
				logIntent.putExtra(Util.EXTRA_DATA, builder.toString());
				LocalBroadcastManager.getInstance(context).sendBroadcast(logIntent);
				resultExtras = new Bundle();
				try {
					resultExtras.putString(AndroidTransport.EXTRA_PACKAGE, context.getPackageName());
					resultExtras.putString(AndroidTransport.EXTRA_CAPABILITIES, CAPABILITIES.toXml());
					this.setResult(AndroidTransport.RESULT_OK, null, resultExtras);
				} catch (Exception e) {
					e.printStackTrace();
					this.setResultCode(AndroidTransport.RESULT_INTERNAL_ERROR);
				}
				break;
			case AndroidTransport.ACTION_TRAFF_POLL:
				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
				boolean respond = preferences.getBoolean(Util.PREF_POLL_RESPOND, false);
				if (respond) {
					builder.append("\nResponding");
					Util.sendFeed(context);
				} else
					builder.append("\nResponding to poll requests is disabled, ignoring");
				Log.d(TAG, builder.toString());
				logIntent.putExtra(Util.EXTRA_DATA, builder.toString());
				LocalBroadcastManager.getInstance(context).sendBroadcast(logIntent);
				break;
			case AndroidTransport.ACTION_TRAFF_SUBSCRIBE:
				String packageName = intent.getStringExtra(AndroidTransport.EXTRA_PACKAGE);
				if (packageName != null)
					builder.append("\n").append(packageName);
				builder.append("\n").append(intent.getStringExtra(AndroidTransport.EXTRA_FILTER_LIST));
				try {
					filterList = FilterList.read(intent.getStringExtra(AndroidTransport.EXTRA_FILTER_LIST));
				} catch (Exception e) {
					// nothing to do here, we will handle this later
				}
				if ((packageName == null) || (filterList == null)) {
					builder.append("\nInvalid request");
					this.setResultCode(AndroidTransport.RESULT_INVALID);
				} else {
					builder.append("\nReturning dummy subscription ID");
					resultExtras = new Bundle();
					resultExtras.putString(AndroidTransport.EXTRA_PACKAGE, context.getPackageName());
					resultExtras.putString(AndroidTransport.EXTRA_SUBSCRIPTION_ID, Util.SUBSCRIPTION_ID_DUMMY);
					this.setResult(AndroidTransport.RESULT_OK, Util.CONTENT_URI, resultExtras);
				}
				Log.d(TAG, builder.toString());
				logIntent.putExtra(Util.EXTRA_DATA, builder.toString());
				LocalBroadcastManager.getInstance(context).sendBroadcast(logIntent);
				break;
			case AndroidTransport.ACTION_TRAFF_SUBSCRIPTION_CHANGE:
				subscriptionId = intent.getStringExtra(AndroidTransport.EXTRA_SUBSCRIPTION_ID);
				if (subscriptionId != null)
					builder.append("\n").append(subscriptionId);
				builder.append("\n").append(intent.getStringExtra(AndroidTransport.EXTRA_FILTER_LIST));
				try {
					filterList = FilterList.read(intent.getStringExtra(AndroidTransport.EXTRA_FILTER_LIST));
				} catch (Exception e) {
					// nothing to do here, we will handle this later
				}
				if ((subscriptionId == null) || (filterList == null)) {
					builder.append("\nInvalid request");
					this.setResultCode(AndroidTransport.RESULT_INVALID);
				} else if (subscriptionId.equals(Util.SUBSCRIPTION_ID_DUMMY)) {
					builder.append("\nSuccess: subscription ID is valid, nothing to do");
					this.setResultCode(AndroidTransport.RESULT_OK);
				} else {
					builder.append("\nSubscription ID is unknown");
					this.setResultCode(AndroidTransport.RESULT_SUBSCRIPTION_UNKNOWN);
				}
				Log.d(TAG, builder.toString());
				logIntent.putExtra(Util.EXTRA_DATA, builder.toString());
				LocalBroadcastManager.getInstance(context).sendBroadcast(logIntent);
				break;
			case AndroidTransport.ACTION_TRAFF_UNSUBSCRIBE:
				subscriptionId = intent.getStringExtra(AndroidTransport.EXTRA_SUBSCRIPTION_ID);
				if (subscriptionId == null) {
					builder.append("\nInvalid request");
					this.setResultCode(AndroidTransport.RESULT_INVALID);
				} else {
					builder.append("\nSuccess");
					// Nothing to do as we don’t track subscriptions, but report success
					this.setResult(AndroidTransport.RESULT_OK, null, null);
				}
				Log.d(TAG, builder.toString());
				logIntent.putExtra(Util.EXTRA_DATA, builder.toString());
				LocalBroadcastManager.getInstance(context).sendBroadcast(logIntent);
				break;
			}
		}
    }
}
