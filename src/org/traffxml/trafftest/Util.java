package org.traffxml.trafftest;

import java.util.Date;
import org.traffxml.traff.IntQuantifier;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffFeed;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffLocation.Directionality;
import org.traffxml.traff.TraffLocation.RoadClass;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.TraffSupplementaryInfo;
import org.traffxml.transport.android.AndroidTransport;

import android.content.Context;
import android.content.Intent;

/**
 * Utility functions
 */
public class Util {
	/**
	 * Internal intent to notify the main activity of log data
	 */
	public static final String ACTION_INTERNAL_LOG = "org.traffxml.roadeagle.LOG";

    /**
     * Authority for the test content provider
     */
    public static final String CONTENT_AUTHORITY = "org.traffxml.trafftest.MessageProvider";

	/**
	 * Extra for {@link #ACTION_INTERNAL_LOG} specifying the data to be logged.
	 * 
	 * This is a String extra.
	 */
	public static final String EXTRA_DATA = "data";

    public static String PREF_POLL_RESPOND = "pref_poll_respond";

    /**
     * Permission request to access coarse location
     */
    public static final int PERM_REQUEST_LOCATION = 1;

    /**
     * Dummy subscription ID used by the test source
     */
	public static final String SUBSCRIPTION_ID_DUMMY = "dummy_subscription_which_will_never_return_messages";

	/**
	 * Authority for the test content provider
	 */
	public static final String CONTENT_URI = String.format("%s://%s/%s", AndroidTransport.CONTENT_SCHEMA, CONTENT_AUTHORITY, SUBSCRIPTION_ID_DUMMY);

    /**
     * Sends a traffic feed.
     */
    static void sendFeed(Context context) {
        TraffFeed feed;

        /* Create a sample feed and convert it to text */
        feed = generateFeed(null, null, null);
        String feedString;
        try {
        	feedString = feed.toXml();

        	/* Create a new intent and put the message in as an extra */
        	Intent outIntent = new Intent(AndroidTransport.ACTION_TRAFF_PUSH);
        	outIntent.putExtra(AndroidTransport.EXTRA_PACKAGE, context.getPackageName());
        	outIntent.putExtra(AndroidTransport.EXTRA_FEED, feedString);

        	/* Send broadcast intent */
        	context.sendBroadcast(outIntent);
        } catch (Exception e) {
        	// TODO Auto-generated catch block
        	e.printStackTrace();
        }
    }

    /**
     * @brief Generates a sample TraFF feed.
     * 
     * @param earlier The timestamp to use for past events; default is 5 minutes before {@code now}
     * @param now The timestamp to use for the last update; default is current time
     * @param later The timestamp to use for future events (expiration or end time); default is 30 minutes after {@code now}
     * @return
     */
    private static TraffFeed generateFeed(Date earlier, Date now, Date later) {
        TraffFeed res = new TraffFeed();
        if (now == null)
            now = new Date(System.currentTimeMillis());
        if (earlier == null)
            earlier = new Date(now.getTime() - 300000);
        if (later == null)
            later = new Date(now.getTime() + 1800000);
        TraffMessage.Builder messageBuilder;
        TraffEvent.Builder eventBuilder;
        TraffLocation.Builder locationBuilder;

        messageBuilder = new TraffMessage.Builder();
        messageBuilder.setId("test:simple");
        messageBuilder.setReceiveTime(earlier);
        messageBuilder.setUpdateTime(now);
        messageBuilder.setExpirationTime(later);
        messageBuilder.addReplaces("test:old");
        locationBuilder = new TraffLocation.Builder();
        locationBuilder.setCountry("LT");
        locationBuilder.setOrigin("Klaipėda");
        locationBuilder.setDestination("Vilnius");
        locationBuilder.setDirectionality(Directionality.ONE_DIRECTION);
        locationBuilder.setRoadClass(RoadClass.MOTORWAY);
        locationBuilder.setRoadRef("A1");
        locationBuilder.setFrom(new TraffLocation.Point(55.707481f, 21.550529f, "Vėžaičiai", null));
        locationBuilder.setTo(new TraffLocation.Point(55.687099f, 21.691055f, "Endriejavas", null));
        messageBuilder.setLocation(locationBuilder.build());
        eventBuilder = new TraffEvent.Builder();
        eventBuilder.setEventClass(TraffEvent.Class.RESTRICTION);
        eventBuilder.setType(TraffEvent.Type.RESTRICTION_LANE_BLOCKED);
        eventBuilder.setQuantifier(new IntQuantifier(1));
        messageBuilder.addEvent(eventBuilder.build());
        eventBuilder = new TraffEvent.Builder();
        eventBuilder.setEventClass(TraffEvent.Class.CONGESTION);
        eventBuilder.setType(TraffEvent.Type.CONGESTION_TRAFFIC_PROBLEM);
        eventBuilder.setSpeed(50);
        eventBuilder.addSupplementaryInfo(new TraffSupplementaryInfo(TraffSupplementaryInfo.Class.TENDENCY,
                TraffSupplementaryInfo.Type.S_TENDENCY_QUEUE_DECREASING));
        messageBuilder.addEvent(eventBuilder.build());
        res.getMessages().add(messageBuilder.build());

        // TODO generate more messages for feed
        return res;
    }
}
